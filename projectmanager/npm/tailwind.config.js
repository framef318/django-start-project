/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  content: [
    "../templates/**/*.{html,js,css}",
    "../templates/*.{html,js,css}",
    "./templates/**/*.{html,js,css}",
    "./templates/*.{html,js,css}",
    "./node_modules/flowbite/**/*.js",
    '../node_modules/flowbite/**/*.js'
    // "../member/forms.py",
  ],
  theme: {
    colors: {
      'custom-green': '#06C755',
      primary: { "50": "#eff6ff", "100": "#dbeafe", "200": "#bfdbfe", "300": "#93c5fd", "400": "#60a5fa", "500": "#3b82f6", "600": "#2563eb", "700": "#1d4ed8", "800": "#1e40af", "900": "#1e3a8a", "950": "#172554" }
    },
    fontFamily: {
    },
    extend: {
    }
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('flowbite-typography'),
    require('flowbite/plugin'),
  ],
}
