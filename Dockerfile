FROM ubuntu:22.04

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=projectmanager.settings

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get -y install curl \
    && curl -s https://deb.nodesource.com/setup_18.x | bash \
    && apt-get install -y --no-install-recommends \
    build-essential \
    python3-dev \
    libpq-dev \
    nodejs \
    python3-pip \ 
    && python3 -m pip install --upgrade pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /app/requirements.txt
COPY ./projectmanager/. /app/

WORKDIR /app/npm
RUN rm -rf node_modules package-lock.json \
    && npm install \
    && npm run build

WORKDIR /app
RUN pip install --no-cache-dir -r requirements.txt \
    && python3 manage.py collectstatic --noinput

EXPOSE 8080
